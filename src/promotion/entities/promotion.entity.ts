import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  condition: string;

  @Column()
  startDate: string;

  @Column()
  endDate: string;
}
