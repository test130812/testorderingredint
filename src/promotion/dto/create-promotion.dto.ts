export class CreatePromotionDto {
  condition: string;
  startDate: string;
  endDate: string;
}
