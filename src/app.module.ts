import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PromotionModule } from './promotion/promotion.module';
import { MemberModule } from './member/member.module';
import { BranchModule } from './branch/branch.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Branch } from './branch/entities/branch.entity';
import { DataSource } from 'typeorm';
import { Promotion } from './promotion/entities/promotion.entity';
import { OrderIngredient } from './order-ingredient/entities/order-ingredient.entity';
import { OrderIngredientModule } from './order-ingredient/order-ingredient.module';
import { OrderIngredientItem } from './order-ingredient/entities/orderItem-ingredient.entity';
import { IngredientStoreModule } from './ingredient-store/ingredient-store.module';
import { IngredientStore } from './ingredient-store/entities/ingredient-store.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'Work_group2_DB.sqlite',
      entities: [
        Branch,
        Promotion,
        OrderIngredient,
        OrderIngredientItem,
        IngredientStore,
      ],
      synchronize: true,
    }),
    PromotionModule,
    MemberModule,
    BranchModule,
    OrderIngredientModule,
    IngredientStoreModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
