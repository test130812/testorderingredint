export class Member {
  id: number;
  name: string;
  tel: string;
  createdDate: Date;
  [key: string]: number | string | Date;
}
