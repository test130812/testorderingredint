import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { Member } from './entities/member.entity';

@Injectable()
export class MemberService {
  lastId: number = 5;
  member: Member[] = [
    {
      id: 1,
      name: 'Kurt Cobain',
      tel: '0912345678',
      createdDate: new Date('01/01/2024'),
    },
    {
      id: 2,
      name: 'Axl Rose',
      tel: '0911222266',
      createdDate: new Date('01/01/2024'),
    },
    {
      id: 3,
      name: 'Freddie Mercury',
      tel: '0913332222',
      createdDate: new Date('01/01/2024'),
    },
    {
      id: 4,
      name: 'Jimi Hendrix',
      tel: '0811212252',
      createdDate: new Date('01/01/2024'),
    },
    {
      id: 5,
      name: 'Anthony Kiedis',
      tel: '0875556666',
      createdDate: new Date('01/01/2024'),
    },
  ];

  create(createMemberDto: CreateMemberDto) {
    this.lastId++;
    const newMember = { ...createMemberDto, id: this.lastId };
    this.member.push(newMember);
    return createMemberDto;
  }

  findAll() {
    return this.member;
  }

  findOne(id: number) {
    const index = this.member.findIndex((member) => member.id === id);
    if (index < 0) {
      throw new NotFoundException();
    }
    return this.member[index];
  }

  update(id: number, updateMemberDto: UpdateMemberDto) {
    const index = this.member.findIndex((member) => member.id === id);
    if (index < 0) {
      throw new NotFoundException();
    }
    this.member[index] = {
      ...this.member[index],
      ...updateMemberDto,
    };
    return this.member[index];
  }

  remove(id: number) {
    const index = this.member.findIndex((member) => member.id === id);
    if (index < 0) {
      throw new NotFoundException();
    }
    const delMember = this.member[index];
    this.member.splice(index, 1);
    return delMember;
  }
}
