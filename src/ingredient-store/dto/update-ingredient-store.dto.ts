import { PartialType } from '@nestjs/mapped-types';
import { CreateIngredientStoreDto } from './create-ingredient-store.dto';

export class UpdateIngredientStoreDto extends PartialType(
  CreateIngredientStoreDto,
) {}
