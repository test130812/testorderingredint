import { Test, TestingModule } from '@nestjs/testing';
import { IngredientStoreController } from './ingredient-store.controller';
import { IngredientStoreService } from './ingredient-store.service';

describe('IngredientStoreController', () => {
  let controller: IngredientStoreController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [IngredientStoreController],
      providers: [IngredientStoreService],
    }).compile();

    controller = module.get<IngredientStoreController>(
      IngredientStoreController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
