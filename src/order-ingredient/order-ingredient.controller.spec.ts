import { Test, TestingModule } from '@nestjs/testing';
import { OrderIngredientController } from './order-ingredient.controller';
import { OrderIngredientService } from './order-ingredient.service';

describe('OrderIngredientController', () => {
  let controller: OrderIngredientController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OrderIngredientController],
      providers: [OrderIngredientService],
    }).compile();

    controller = module.get<OrderIngredientController>(
      OrderIngredientController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
