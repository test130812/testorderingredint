import { Injectable } from '@nestjs/common';
import { CreateOrderIngredientDto } from './dto/create-order-ingredient.dto';
import { UpdateOrderIngredientDto } from './dto/update-order-ingredient.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { OrderIngredient } from './entities/order-ingredient.entity';
import { Repository } from 'typeorm';
import { OrderIngredientItem } from './entities/orderItem-ingredient.entity';
import { IngredientStore } from 'src/ingredient-store/entities/ingredient-store.entity';

@Injectable()
export class OrderIngredientService {
  constructor(
    @InjectRepository(OrderIngredient)
    private ordersIngredientRepository: Repository<OrderIngredient>,
    @InjectRepository(OrderIngredientItem)
    private orderIngredientItemRepository: Repository<OrderIngredientItem>,
    @InjectRepository(IngredientStore)
    private ingredientStoreRepository: Repository<IngredientStore>,
  ) {}
  async create(createOrderIngredientDto: CreateOrderIngredientDto) {
    const orderIngredient = new OrderIngredient();
    //const user
    //order.user = user;
    //orderIngredient.memberDiscount = 0;
    orderIngredient.totalBefore = 0;
    orderIngredient.total = 0;
    orderIngredient.totalAmount = 0;
    orderIngredient.receivedAmount = 0;
    orderIngredient.change = 0;
    orderIngredient.paymentType = 'Cash';
    orderIngredient.orderIngredientItems = [];
    for (const oi of createOrderIngredientDto.orderIngredientItems) {
      const orderIngredientItem = new OrderIngredientItem();
      console.log('1');
      const ingredientStore = await this.ingredientStoreRepository.findOneBy({
        id: oi.ingredientId,
      });
      orderIngredientItem.ingredientStore = ingredientStore;
      orderIngredientItem.name = ingredientStore.name;
      orderIngredientItem.acc = ingredientStore.price;
      orderIngredientItem.price = ingredientStore.price * oi.qty;
      orderIngredientItem.qty = oi.qty;
      orderIngredientItem.unit = ingredientStore.unit;
      await this.orderIngredientItemRepository.save(orderIngredientItem);
      orderIngredient.orderIngredientItems.push(orderIngredientItem);
      orderIngredient.total += orderIngredientItem.price;
      orderIngredient.totalAmount += orderIngredientItem.qty;
      console.log('55');
    }
    return this.ordersIngredientRepository.save(orderIngredient);
  }

  findAll() {
    return this.ordersIngredientRepository.find({
      relations: { orderIngredientItems: true },
      order: {
        id: 'desc',
      },
    });
  }

  findOne(id: number) {
    return this.ordersIngredientRepository.findOneOrFail({
      where: { id },
      relations: { orderIngredientItems: true },
    });
  }

  update(id: number, updateOrderIngredientDto: UpdateOrderIngredientDto) {
    return `This action updates a #${id} orderIngredient`;
  }

  async remove(id: number) {
    const deleteOrder = await this.ordersIngredientRepository.findOneOrFail({
      where: { id },
    });
    await this.ordersIngredientRepository.remove(deleteOrder);

    return deleteOrder;
  }
}
